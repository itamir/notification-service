package br.com.imd.notificationexample;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.Context;
import android.support.v4.app.NotificationCompat;

public class NotificationService extends IntentService {

    public NotificationService() {
        super("NotificationService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        while (true) {
            try {
                Thread.sleep(10000);

                Intent notificationIntent = new Intent("br.ufrn.imd.notification");
                PendingIntent contentIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);


                NotificationCompat.Builder mBuilder =
                        new NotificationCompat.Builder(this)
                                .setSmallIcon(R.mipmap.ic_launcher)
                                .setContentTitle("Teste de notificação.")
                                .setContentText("Aqui está sua notificação!")
                                .setVibrate(new long[] {100, 250})
                                .setContentIntent(contentIntent);

                NotificationManager mNotificationManager =
                        (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

                mNotificationManager.notify(1, mBuilder.build());


                Thread.sleep(10000);

                mNotificationManager.cancelAll();

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}
